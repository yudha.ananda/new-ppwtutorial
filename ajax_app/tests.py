from django.test import TestCase , Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest,HttpResponse

from ajax_app import views


# Create your tests here.

class Lab9UnitTest(TestCase):

    # check url for books
    def test_url_used(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    # check template used for showing books
    def test_template_used(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response,'index_books.html')

    # check views.py function used in that URL
    def test_lab_9_func(self):
        response_func = resolve('/books/')
        self.assertEqual(response_func.func, views.show_books)

    # check views.py function return json data of books
    def test_lab_9_books_json(self):
        response_func = resolve('/books/api-data-books/')
        self.assertEqual(response_func.func,views.data_books)




