from django.urls import path
from ajax_app import views


urlpatterns = [
    path('', views.show_books, name="show_books"),
    path('api-data-books/',views.data_books,name="data_books")
]