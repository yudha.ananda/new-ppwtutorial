from django.shortcuts import render, redirect
from django.core import serializers
from django.http import HttpResponse, HttpRequest, HttpResponseBadRequest
import requests
import json


search = "quilting" #default
# show books html page views
def show_books(request):
     if request.method == "POST":
         retrieve_endpoint = request.POST.get('type_books')
         global search
         search = retrieve_endpoint
         return redirect('show_books')
     else:
        return render(request, 'index_books.html')


# json books data
def data_books(request):
    books_endpoint = search
    book_api = 'https://www.googleapis.com/books/v1/volumes?q='+books_endpoint
    get_json_obj = requests.get(book_api)
    result = (get_json_obj.json())['items']
    return_data = {"data": result } # for datatables
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")   
