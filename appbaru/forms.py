from django import forms
from .models import modelstatus

class formstatus(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    status = forms.CharField(max_length = 300, required = True, widget = forms.TextInput())