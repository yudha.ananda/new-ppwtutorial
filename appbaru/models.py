from django.db import models
from django.utils import timezone
from datetime import date, time

class modelstatus(models.Model):
    status = models.CharField(max_length = 300)
    date_created = models.DateTimeField(auto_now_add=True)