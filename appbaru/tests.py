import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profil
from .views import status
from .models import modelstatus
from .forms import formstatus
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options



# Unit test
class Story7UnitTest(TestCase):

    def test_lab6_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_url_is_not_exist(self):
        response = Client().get('/NotExistPage')
        self.assertEqual(response.status_code, 404)

    def test_lab6_using_index(self):
        found = resolve('/appbaru')
        self.assertEqual(found.func, profil)

    def test_model_can_create_status(self):
        status = modelstatus.objects.create(status = "would you be mine?")
        counting_all_status = modelstatus.objects.all().count()
        self.assertEqual(counting_all_status,1)
    
    def test_profil_url_is_not_exist(self):
        response = Client().get('/profil')
        self.assertEqual(response.status_code, 200)

    def test_profil_url_is_not_exist(self):
        response = Client().get('/NotExistPage')
        self.assertEqual(response.status_code, 404)

    def test_get_char_in_profile(self):
    	response = Client().get("/profil")
    	html_response = response.content.decode('utf8')
    	self.assertIn("Hi, aku Yudha!", html_response)

    def test_get_img_in_profile(self):
    	response = Client().get("/profil")
    	html_response = response.content.decode('utf8')
    	self.assertIn("https://image.ibb.co/j6PdMe/660648.jpg", html_response)

    def test_get_charfoot_in_profile(self):
    	response = Client().get("/profil")
    	html_response = response.content.decode('utf8')
    	self.assertIn("Hubungi Saya :", html_response)



# Functional test
# class Story7FunctionalTest(TestCase):

#     # Menyiapkan driver untuk chrome
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser = webdriver.Chrome('./chromedriver',
#                                         chrome_options=chrome_options)
#         super(Story7FunctionalTest, self).setUp()

#    # Menutup browser
#     def tearDown(self):
#         self.browser.quit()
#         super(Story7FunctionalTest, self).tearDown()

#     def test_input_status_field(self):
#         # Selenium adalah sebuah browser yang akan di tes melalui paradigma functional test
#         driver = self.browser

#         # Membuka link yang akan dites
#         driver.get('localhost:8000/status')

#         # Menemukan elemen-elemen yang terdapat di halaman
#         statusfield_coloumn = driver.find_element_by_id('id_status')
#         submit = driver.find_element_by_id('submitbutton')

#         # Memasukkan data ke dalam from
#         statusfield_coloumn.send_keys('Coba Coba')

#         # Submit form dengan data
#         submit.send_keys(Keys.RETURN)
#         time.sleep(5)
#         response = Client().get('localhost:8000/status')
#         self.assertIn('Coba Coba', self.browser.page_source)

#     def test_page_title(self):
#          # Membuka halaman
#          driver = self.browser
#          driver.get('localhost:8000/status')

#          #Mencari judul
#          self.assertIn('Hello, Apa kabar?', self.browser.page_source)

#     def test_find_footer_element(self):
#         # Membuka halaman
#          driver = self.browser
#          driver.get('localhost:8000/')

#          #Mencari judul
#          self.assertIn('Hubungi Saya :', self.browser.page_source)

#     def test_styling_footer_with_bootstrap(self):
#          # Membuka halaman
#          driver = self.browser
#          driver.get('localhost:8000/')

#          # Mengecek style footer
#          footer = driver.find_element_by_tag_name('footer')
#          footer_style = 'page-footer font-small cyan darken-3 footer' in footer.get_attribute("class")

#     def test_styling_title_page(self):
#         # Membuka halaman
#          driver = self.browser
#          driver.get('localhost:8000/status')

#         # Mengecek style judul
#          title = driver.find_element_by_tag_name('h2')
#          title_style = 'judul' in title.get_attribute("class")