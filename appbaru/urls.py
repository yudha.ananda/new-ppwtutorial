from django.urls import path
from .views import status, profil
from django.conf import settings

urlpatterns = [
    path('status', status),
    path('profil', profil),
	path('', profil)
]

