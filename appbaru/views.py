from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import formstatus
from .models import modelstatus

# Create your views here.
response = {'author' : 'Yudha Ananda'}

def status(request):
	mdlstatus = modelstatus.objects.all()
	frmstatus = formstatus
	if request.method == 'POST':
		if "hapus" in request.POST:
			mdlstatus.delete()
			context = {'frmstatus' : frmstatus, 'mdlstatus' : mdlstatus}
			return render(request, 'home.html', context)
		
		response['status'] = request.POST['status']
		stat = modelstatus(status = response['status'])
		stat.save()
		form = formstatus()
		context = {
			'frmstatus' : frmstatus,
			'mdlstatus' : mdlstatus
		}
		return render(request, "home.html", context)

	else :
		form = formstatus()
		context = {
			'frmstatus' : frmstatus,
			'mdlstatus' : mdlstatus
		}
		return render(request, "home.html", context)

def profil(request):
	return render(request, "profil.html")