from django.contrib import admin
from .models import RegisterSubscribe

# Register your models here.

class RegisterSubcribeAdmin(admin.ModelAdmin):
     list_display= ['pk','email', 'nama', 'password']

admin.site.register(RegisterSubscribe,RegisterSubcribeAdmin)
