from django.test import TestCase, Client
from django.urls import resolve
from lab10 import views
from .models import RegisterSubscribe

import json

# Test Driven Development Test lab_10
class Lab10TestCase(TestCase):

    # check if url OK status for subscribe page
    def test_url_used_10(self):
        response = Client().get('/welcome/subscribe/')
        self.assertEqual(response.status_code , 200)

    # check if func in views.py lab_10 used
    def test_func_used_10(self):
        response = resolve('/welcome/subscribe/')
        self.assertEqual(response.func , views.subscribe)

    # check if template html lab_10 used
    def test_template_used_10(self):
        response = Client().get('/welcome/subscribe/')
        self.assertTemplateUsed(response,'subscribe_page.html')

    # check if user can create account
    def test_user_create_account(self):
        response = RegisterSubscribe.objects.create(nama="Yudha Ananda", email="yudha@gmail.com",password="123456")
        count_account = RegisterSubscribe.objects.all().count()
        self.assertEqual(count_account,1)

    # check validate user account
    def test_validate_function(self):
        response = resolve('/welcome/validate/')
        self.assertEqual(response.func , views.user_validate)

    # check validate name account

    def test_email_valid(self):
        data = {

            "nama":"",
            "email": "",
            "password": ""
        }
        response = Client().post('/welcome/validate/', data)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'] , 'email must contain "@" character')


    # check validate email account
    def test_name_valid(self):
        data = {

            "nama": "test",
            "email": "asdasdasdsadas",
            "password": "dasdasdas"
        }
        response = Client().post('/welcome/validate/', data)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'], 'Name must be 5 to 30 characters or fewer.')

    # check validate password account

    def test_email_valid(self):
        data = {

            "nama": "test",
            "email": "wkwkwk@gmail.com",
            "password": "sdasd"
        }
        response = Client().post('/welcome/validate/' ,data)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'], 'Name must be 5 to 30 characters or fewer.')

    # check response submit form
    def test_subscribe_form_response_valid(self):
        data_form = {

            'nama' : "Babang Nuga",
            'email' : "babangnuga@gmail.com",
            'password':"waktudulukala"
        }
        response = Client().post('/welcome/validate/' , data_form)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'] , "All fields are valid")


    # check json response
    def test_json_response_validate(self):
        response = Client().get('/welcome/validate/' ,{'email': 'babon99@gmail.com'})
        result = json.loads(response.content.decode('utf-8'))
        check_output = result['message']
        print(check_output)
        self.assertEqual(check_output,"There's something wrong. Try again." )
