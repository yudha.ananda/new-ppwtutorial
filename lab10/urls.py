from django.urls import path
from lab10 import views

urlpatterns = [
    path('subscribe/', views.subscribe , name="subscribe_url"),
    path('validate/', views.user_validate , name="validate_email"),
    path('unsubscribe/' , views.unsubscribe , name="unsubscribe_email")
]