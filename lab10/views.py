from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from .forms import RegisterSubscribeForm
from .models import RegisterSubscribe
from django.core.paginator import Paginator
import requests
import json
import re


# Create your views here.

# function POST request subscriber
@csrf_exempt
def subscribe(request):

    # pagination page
    get_user_acccount = RegisterSubscribe.objects.all()
    paginator = Paginator(get_user_acccount, 8)
    page = request.GET.get('page')
    get_user_paginate = paginator.get_page(page)

    # post method save user
    if request.method == "POST":
        form_value = RegisterSubscribeForm(request.POST or None)
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        create_user = RegisterSubscribe(email=email, nama=nama, password=password)
        create_user.save()

    else:
        form_value = RegisterSubscribeForm()

    return render(request, 'subscribe_page.html', {'the_form': form_value, 'user_account': get_user_paginate})


# function for validate email user , name , password 
@csrf_exempt
def user_validate(request):

    # boolean flag for chack each form
    nama_validate_check = False
    password_validate_check = False
    email_validate_check = False

    if request.method == "POST":
        email = request.POST['email']
        nama = request.POST['nama']
        password = request.POST['password']

        # using regex check validate email address
        if len(email) >= 8:
            email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email))

        if 8 <= len(password) <= 14:
            password_validate_check = True

        if 5 <= len(nama) <= 30:
            nama_validate_check = True

        if not nama_validate_check:
            return HttpResponse(json.dumps({'message': 'Name must be 5 to 30 characters or fewer.'}),
                                content_type="application/json")

        if not email_validate_check:
            return HttpResponse(json.dumps({'message': 'email must contain "@" character'}),
                                content_type="application/json")

        if not password_validate_check:
            return HttpResponse(json.dumps({'message': 'Your password must contain at least 8 characters'}),
                                content_type="application/json")

        user_filter_email = RegisterSubscribe.objects.filter(email=email)

        if len(user_filter_email) > 0:
            return HttpResponse(json.dumps({'message': 'Email already registered'}), content_type="application/json")

        return HttpResponse(json.dumps({'message': 'All fields are valid'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),
                            content_type="application/json")


@csrf_exempt
def unsubscribe(request):
    if request.method == "POST":
        part_id = request.POST['id']
        data_user = RegisterSubscribe.objects.get(pk=part_id)
        data_user.delete()
        return HttpResponse(json.dumps({'message': "Delete successful"}), content_type="application/json")

    else:
        return render(request, 'subscribe_page.html')
